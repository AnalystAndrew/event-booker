package com.analystplatform.seatbooker.models;

import java.util.List;

import com.analystplatform.seatbooker.ApplicationConfig;

/**
 * Contains information related to holding seats in a venue.
 */
public class SeatHold {

    private int id;
    private String email;
    private List<Seat> seats;
    private long expirationTime;

    public SeatHold() {
        initExpiration(ApplicationConfig.MAX_SECONDS_TO_HOLD_SEATS);
    }

    void initExpiration(int secondsToExpire) {
        this.expirationTime = System.currentTimeMillis() + secondsToExpire * 1000;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Seat> getSeats() {
        return seats;
    }

    public void setSeats(List<Seat> seats) {
        this.seats = seats;
    }

    public boolean isExpired() {
        return System.currentTimeMillis() > expirationTime;
    }

    long getExpirationTime() {
        return this.expirationTime;
    }

    public void setExpirationTime(long expirationTime) {
        this.expirationTime = expirationTime;
    }

    @Override
    public String toString() {
        return "SeatHold [id=" + id + ", email=" + email + ", seats=" + seats + "]";
    }

}
