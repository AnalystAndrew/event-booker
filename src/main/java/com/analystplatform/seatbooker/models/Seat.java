package com.analystplatform.seatbooker.models;

/**
 * Represents a Seat in which a person sits at a venue.
 */
public class Seat {

    private int row;
    private int column;

    private boolean isHeld;
    private boolean isReserved;

    public boolean isHeld() {
        return isHeld;
    }

    public void setHeld(boolean isHeld) {
        this.isHeld = isHeld;
    }

    public boolean isReserved() {
        return isReserved;
    }

    public void setReserved(boolean isReserved) {
        this.isReserved = isReserved;
    }

    public boolean isOpen() {
        return !(isHeld || isReserved);
    }

    public int getRow() {
        return row;
    }

    public Seat setRow(int row) {
        this.row = row;
        return this;
    }

    public int getColumn() {
        return column;
    }

    public Seat setColumn(int column) {
        this.column = column;
        return this;
    }

    @Override
    public String toString() {
        return "Seat [row=" + row + ", column=" + column + "]";
    }

}
