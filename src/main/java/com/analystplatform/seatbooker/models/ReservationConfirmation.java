package com.analystplatform.seatbooker.models;

import java.util.List;

public class ReservationConfirmation {
    private String id;
    private String email;
    private List<Seat> seats;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Seat> getSeats() {
        return seats;
    }

    public void setSeats(List<Seat> seats) {
        this.seats = seats;
    }

    @Override
    public String toString() {
        return "ReservationConfirmation [id=" + id + ", email=" + email + ", seats=" + seats + "]";
    }

}
