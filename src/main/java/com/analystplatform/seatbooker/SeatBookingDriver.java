package com.analystplatform.seatbooker;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import com.analystplatform.seatbooker.models.ReservationConfirmation;
import com.analystplatform.seatbooker.models.Seat;
import com.analystplatform.seatbooker.models.SeatHold;

/** Main entry point to the application. Launches the app. */
public class SeatBookingDriver {

    private SeatBookingEngine engine = new SeatBookingEngine();

    public void run() {
        try (Scanner scanner = new Scanner(System.in)) {
            int input = 0;
            do {
                printMenu();
                try {
                    input = scanner.nextInt();
                } catch (InputMismatchException e) {
                    System.err.println("Please enter a valid number");
                    scanner.nextLine();
                }
                clearScreen();
                switch (input) {
                case 1:
                    printAvailableSeatsCount(scanner);
                    break;
                case 2:
                    holdSeats(scanner);
                    break;
                case 3:
                    printHeldSeats();
                    break;
                case 4:
                    reserveSeats(scanner);
                    break;
                case 5:
                    printReservations();
                    break;
                }

            } while (input != 9);
        }
    }

    private void printMenu() {
        System.out.println("\nChoose an option from the menu below:");
        System.out.println("  1.) Print available seat count");
        System.out.println("  2.) Hold a given number of seats");
        System.out.println("  3.) Print held seats");
        System.out.println("  4.) Reserve held seats");
        System.out.println("  5.) Print reservations");
        System.out.println("  ");
        System.out.println("  9.) Exit\n");
    }

    private void holdSeats(Scanner scanner) {
        System.out.println("Hold seats!");
        System.out.println("How many seats to reserve?");
        int numSeats = 0;
        int maxAvailableSeats = engine.numSeatsAvailable();
        if (maxAvailableSeats <= 0) {
            System.out.println("No seats are available to be held.  Maybe some will open up later");
            return;
        }
        while (numSeats <= 0 || numSeats > maxAvailableSeats) {
            try {
                System.out.println("Please enter a number less than " + maxAvailableSeats);
                numSeats = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.err.println("Please enter a valid number");
                scanner.nextLine();
            }
        }

        System.out.println("What email to register them under?");
        String email = "";
        while (email.length() < 1 || email.length() > 1000) {
            email = scanner.next();
        }

        System.out.println("Saving " + numSeats + " seats for " + email);
        engine.findAndHoldSeats(numSeats, email);
    }

    private void printHeldSeats() {
        List<SeatHold> heldSeats = engine.getHeldSeats();
        System.out.println("Number of seat holdings: " + heldSeats.size());
        heldSeats.forEach((hold) -> {
        	System.out.printf("Reservation confirmation:%n");
        	System.out.printf("\tId:\t%d%n", hold.getId());
        	System.out.printf("\tEmail:\t%s%n", hold.getEmail());
        	System.out.printf("\tSeats:\t%s%n%n", String.join(", ", hold.getSeats().stream().map((seat) -> String.format("(%d, %d)", seat.getRow(), seat.getColumn())).collect(Collectors.toList())));
        });
    }

    private void reserveSeats(Scanner scanner) {
        System.out.println("Reserve held seats!");
        System.out.println("Enter the seat hold ID: ");
        int seatHoldId = 0;
        while (seatHoldId <= 0) {
            try {
                seatHoldId = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.err.println("Please enter a valid seat hold ID");
                scanner.nextLine();
            }
        }

        System.out.println("Enter the email under which the seats were held:");
        String email = "";
        while (email.length() < 1 || email.length() > 1000) {
            email = scanner.next();
        }

        String reservationId = engine.reserveSeats(seatHoldId, email);

        System.out.println(reservationId);
        if (!reservationId.equals(SeatBookingEngine.HOLD_FOR_DIFFERENT_CUSTOMER_MESSAGE)
                        && !reservationId.equals(SeatBookingEngine.HOLD_NOT_FOUND_MESSAGE)) {
            System.out.println("The seats were reserved!  The reservation confirmation number is: " + reservationId);
        }

    }

    private void printReservations() {
        List<ReservationConfirmation> reservations = engine.getReservationConfirmations();
        System.out.println("Number of seat reservations: " + reservations.size());
        reservations.forEach((reservation) -> {
        	System.out.printf("Reservation confirmation:%n");
        	System.out.printf("\tId:\t%s%n", reservation.getId());
        	System.out.printf("\tEmail:\t%s%n", reservation.getEmail());
        	System.out.printf("\tSeats:\t%s%n%n", String.join(", ", reservation.getSeats().stream().map((seat) -> String.format("(%d, %d)", seat.getRow(), seat.getColumn())).collect(Collectors.toList())));
        });

    }

    private void printAvailableSeatsCount(Scanner scanner) {
        printSeats();
        System.out.println(String.format("The number of open seats is: %s", engine.numSeatsAvailable()));

    }

    private void clearScreen() {
        for (int i = 0; i < 10; i++) {
            System.out.println();
        }
    }

    private void printSeats() {
        System.out.println("h = held seat  r = reserved seat  '.' = open seat");
        for (Seat[] row : engine.dao.getSeats()) {
            for (Seat seat : row) {
                String value = "h";
                if (seat.isOpen()) {
                    value = ".";
                }
                if (seat.isReserved()) {
                    value = "r";
                }
                System.out.print(value);
            }
            System.out.println();
        }
    }

    public static void main(String... args) {
        System.out.println("Welcome to the event booking engine!");

        new SeatBookingDriver().run();

        System.out.println("Thanks for using the booking engine!");
        System.exit(0);
    }
}
