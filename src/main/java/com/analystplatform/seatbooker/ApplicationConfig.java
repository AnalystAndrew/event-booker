package com.analystplatform.seatbooker;

/**
 * Class to centralize configuration settings, similar to what may occur via property files or externalized settings in a database.
 */
public class ApplicationConfig {

    // Don't allow instantiation of configuration class
    private ApplicationConfig() {
        // Nothing to do here
    }

    /** Number of rows of seats in the venue */
    public static final int ROWS = 25;

    /** Number of columns of seats in the venue */
    public static final int COLUMNS = 75;

    /** Number of seconds until a seat 'hold' expires */
    public static final int MAX_SECONDS_TO_HOLD_SEATS = 60;
}
