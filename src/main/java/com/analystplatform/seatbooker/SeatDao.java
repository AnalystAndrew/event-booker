package com.analystplatform.seatbooker;

import static com.analystplatform.seatbooker.ApplicationConfig.COLUMNS;
import static com.analystplatform.seatbooker.ApplicationConfig.ROWS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import com.analystplatform.seatbooker.models.ReservationConfirmation;
import com.analystplatform.seatbooker.models.Seat;
import com.analystplatform.seatbooker.models.SeatHold;

public class SeatDao {

    /** Number of rows in the venue */
    private int numberOfRows;

    /** Number of columns in the venue */
    private int numberOfColumns;

    /** In-memory database of seats. */
    private Seat[][] seats;

    /** Map of IDs to Seat Holds */
    private final Map<Integer, SeatHold> seatHolds = new HashMap<>();

    /** Used to generate random numbers for the seat hold IDs */
    private final static Random RANDOM = new Random();

    /** Lookup of confirmed reservations */
    private Map<String, ReservationConfirmation> reservationConfirmations = new HashMap<>();

    public SeatDao() {
        initializeRepo(ROWS, COLUMNS);
    }

    /** Initializes a new database. Default scope for exposure to tests */
    void initializeRepo(int rows, int columns) {
        seats = new Seat[rows][columns];
        for (int i = 0; i < seats.length; i++) {
            for (int j = 0; j < seats[i].length; j++) {
                seats[i][j] = new Seat().setRow(i).setColumn(j);
            }
        }
        this.numberOfRows = rows;
        this.numberOfColumns = columns;

    }

    /**
     * Helper method really for establishing functionality in the system which will perform the given action for each seat in the array
     * 
     * @param consumer
     *            Consumer for each seat in the venue
     */
    void doForEachSeat(Consumer<Seat> consumer) {
        for (int i = 0; i < seats.length; i++) {
            for (int j = 0; j < seats[i].length; j++) {
                consumer.accept(seats[i][j]);
            }
        }
    }

    /** Default-scope exposure for testing */
    Seat[][] getSeats() {
        return seats;
    }

    /** Helper to expose an exact seat. */
    Seat getSeat(int row, int column) {
        return seats[row][column];
    }

    /** Returns the number of rows in the venue */
    public int getNumberOfRows() {
        return numberOfRows;
    }

    /** Returns the number of columns in the venue */
    public int getNumberOfColumns() {
        return numberOfColumns;
    }

    /** Get all the Seats being held, mapped to the IDs of their holds */
    public Map<Integer, SeatHold> getSeatHolds() {
        return seatHolds;
    }

    /** Removes all the seat holds of the given IDs */
    public void removeAllSeatHolds(List<Integer> seatHoldIds) {
        seatHoldIds.forEach((id) -> seatHolds.remove(id));
    }

    public SeatHold newSeatHold(String customerEmail, List<Seat> seatsToHold) {
        SeatHold hold = new SeatHold();
        hold.setId(generateNewSeatHoldId());
        hold.setSeats(seatsToHold);
        hold.setEmail(customerEmail);
        this.seatHolds.put(hold.getId(), hold);
        return hold;
    }

    private int generateNewSeatHoldId() {
        int randomId = RANDOM.nextInt(Integer.MAX_VALUE);
        while (seatHolds.containsKey(randomId)) {
            randomId = RANDOM.nextInt(Integer.MAX_VALUE);
        }
        return randomId;
    }

    /** Returns a seat hold for the given ID. Returns null if one wasn't found */
    public SeatHold getSeatHold(int id) {
        return seatHolds.get(id);
    }

    /** Removes a seat hold from the database of seat holds */
    public void removeSeatHold(int id) {
        seatHolds.remove(id);
    }

    /** Returns any expired seats into the pool of available seats. */
    public void reclaimExpiredHolds() {
        List<Integer> idsToRemove = new ArrayList<>();
        seatHolds.forEach((holdId, hold) -> {
            if (hold.isExpired()) {
                hold.getSeats().forEach((seat) -> {
                    // Reclaim each seat
                    seat.setHeld(false);
                });
                idsToRemove.add(holdId);
            } // end of the expired check
        });
        removeAllSeatHolds(idsToRemove);
    }

    /** Returns the given confirmation for a valid id, null otherwise */
    public ReservationConfirmation getReservationConfirmation(String confirmationNumber) {
        return reservationConfirmations.get(confirmationNumber);
    }

    /** Saves the given confirmation */
    public void saveReservationConfirmation(ReservationConfirmation confirmation) {
        reservationConfirmations.put(confirmation.getId(), confirmation);
    }

    /** Returns all reservation confirmations as a list */
    public List<ReservationConfirmation> getReservationConfirmations() {
        return reservationConfirmations.entrySet().stream().map((entry) -> entry.getValue()).collect(Collectors.toList());
    }

}
