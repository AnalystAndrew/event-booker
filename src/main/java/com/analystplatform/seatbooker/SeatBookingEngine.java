package com.analystplatform.seatbooker;

import java.util.List;
import java.util.stream.Collectors;

import com.analystplatform.seatbooker.models.ReservationConfirmation;
import com.analystplatform.seatbooker.models.SeatHold;
import com.analystplatform.seatbooker.services.SeatBoookingService;

/**
 * TODO: Implement the SeatBookingEngine according to the instructions in the Readme.    
 */
public class SeatBookingEngine implements SeatBoookingService {

	public static final String HOLD_NOT_FOUND_MESSAGE = "Hold doesn't exist for that ID";
	public static final String HOLD_FOR_DIFFERENT_CUSTOMER_MESSAGE = "That hold is for a different customer";

	/** Reference to the repository storing all our data in memory */
	SeatDao dao = new SeatDao();

	public SeatBookingEngine() {
	}

	@Override
	public int numSeatsAvailable() {
		return 0;
	}

	@Override
	public SeatHold findAndHoldSeats(int numSeats, String customerEmail) {
		return null;
	}

	@Override
	public String reserveSeats(int seatHoldId, String customerEmail) {
		return "";
	}

	public List<SeatHold> getHeldSeats() {
		return dao.getSeatHolds().entrySet().stream().map((entry) -> entry.getValue()).collect(Collectors.toList());
	}

	public List<ReservationConfirmation> getReservationConfirmations() {
		return dao.getReservationConfirmations();
	}

}
