package com.analystplatform.seatbooker.models;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.analystplatform.seatbooker.models.Coordinate;

public class CoordinateTest {

    private Coordinate coordinate;

    @Before
    public void setup() {
        coordinate = new Coordinate();
    }

    @Test
    public void testGetRow() {
        assertEquals(4, coordinate.setRow(4).getRow());
    }

    @Test
    public void testGetColumn() {
        assertEquals(12, coordinate.setColumn(12).getColumn());
    }

}
