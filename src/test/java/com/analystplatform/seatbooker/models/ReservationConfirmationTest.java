package com.analystplatform.seatbooker.models;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.analystplatform.seatbooker.models.ReservationConfirmation;
import com.analystplatform.seatbooker.models.Seat;

public class ReservationConfirmationTest {

    private ReservationConfirmation confirmation;

    @Before
    public void setup() {
        confirmation = new ReservationConfirmation();
    }

    @Test
    public void testGetId() {
        confirmation.setId("abc");
        assertEquals("abc", confirmation.getId());
    }

    @Test
    public void testGetEmail() {
        confirmation.setEmail("def");
        assertEquals("def", confirmation.getEmail());
    }

    @Test
    public void testGetSeats() {
        List<Seat> seats = Arrays.asList(new Seat());
        confirmation.setSeats(seats);
        assertEquals(seats, confirmation.getSeats());
    }
}