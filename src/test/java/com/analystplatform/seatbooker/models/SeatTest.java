package com.analystplatform.seatbooker.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.analystplatform.seatbooker.models.Seat;

public class SeatTest {

    private Seat seat;

    @Before
    public void setup() {
        seat = new Seat();
    }

    @Test
    public void testIsHeldGetHeld() {
        // expected default state
        assertFalse(seat.isHeld());

        // Assert the setter and getter work
        seat.setHeld(true);
        assertTrue(seat.isHeld());
    }

    @Test
    public void testIsReservedGetReserved() {
        // expected default state
        assertFalse(seat.isReserved());

        // Assert the setter and getter work
        seat.setReserved(true);
        assertTrue(seat.isReserved());
    }

    @Test
    public void testIsOpen() {
        // expected default state is open
        assertTrue(seat.isOpen());

        // Seat is not open if it's reserved
        seat.setReserved(true);
        assertFalse(seat.isOpen());

        // Goes back to being open if seat is no longer reserved
        seat.setReserved(false);
        assertTrue(seat.isOpen());

        // Seat is not open if it's held
        seat.setHeld(true);
        assertFalse(seat.isOpen());

        // Seat is open if no longer held
        seat.setHeld(false);
        assertTrue(seat.isOpen());

        // If a seat is both held and reserved, it should not be considered open
        seat.setHeld(true);
        seat.setReserved(true);
        assertFalse(seat.isOpen());
    }

    @Test
    public void testSetGetRows() {
        seat.setRow(5);
        assertEquals(5, seat.getRow());
    }

    @Test
    public void testSetGetColumns() {
        seat.setColumn(6);
        assertEquals(6, seat.getColumn());
    }

}
