package com.analystplatform.seatbooker.models;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.analystplatform.seatbooker.ApplicationConfig;
import com.analystplatform.seatbooker.models.Seat;
import com.analystplatform.seatbooker.models.SeatHold;

public class SeatHoldTest {

    private SeatHold hold;

    @Before
    public void setup() {
        hold = new SeatHold();
    }

    @Test
    public void testGetSetId() {
        hold.setId(54);
        assertEquals(54, hold.getId());
    }

    @Test
    public void testGetSetEmail() {
        hold.setEmail("a@b.com");
        assertEquals("a@b.com", hold.getEmail());
    }

    @Test
    public void testGetSetSeats() {
        List<Seat> seats = Arrays.asList(new Seat(), new Seat());
        hold.setSeats(seats);
        assertEquals(seats, hold.getSeats());
    }

    @Test
    public void testSetsExpirationOnConstruction() throws Exception {
        long lowBound = System.currentTimeMillis();
        // Guarantee the system time will always be different across these operations
        Thread.sleep(1);
        SeatHold hold = new SeatHold();
        long actualValue = hold.getExpirationTime();
        Thread.sleep(1);
        long highBound = System.currentTimeMillis();

        assertTrue(lowBound + ApplicationConfig.MAX_SECONDS_TO_HOLD_SEATS * 1000 < actualValue);
        assertTrue(highBound + ApplicationConfig.MAX_SECONDS_TO_HOLD_SEATS * 1000 > actualValue);
    }

    @Test
    public void testIsExpired() throws Exception {
        assertFalse(hold.isExpired());
        hold.initExpiration(1);
        Thread.sleep(1001);
        assertTrue(hold.isExpired());
    }

    @Test
    public void testSetExpirationTime() throws Exception {
        hold.setExpirationTime(14l);
        assertEquals(14l, hold.getExpirationTime());
    }

}
