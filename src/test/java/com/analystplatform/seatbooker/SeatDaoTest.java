package com.analystplatform.seatbooker;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.analystplatform.seatbooker.ApplicationConfig;
import com.analystplatform.seatbooker.SeatDao;
import com.analystplatform.seatbooker.models.Coordinate;
import com.analystplatform.seatbooker.models.ReservationConfirmation;
import com.analystplatform.seatbooker.models.Seat;
import com.analystplatform.seatbooker.models.SeatHold;

public class SeatDaoTest {

    private SeatDao dao;

    @Before
    public void setup() {
        this.dao = new SeatDao();
    }

    @Test
    public void testSeatRepository_Constructor() {
        // Validate the constructor initializes the database to the configured rows/columns
        assertEquals(ApplicationConfig.ROWS, dao.getSeats().length);
        if (dao.getSeats()[0].length < 1) {
            fail("Ensure there is at least 1 row and column in the config");
        }
        assertEquals(ApplicationConfig.COLUMNS, dao.getSeats()[0].length);
    }

    @Test
    public void initializeRepo() {
        // Validate the repo gets set to the given row and column lengths
        assertNotEquals(15, dao.getSeats().length);
        assertNotEquals(15, dao.getSeats()[0].length);

        dao.initializeRepo(20, 30);
        assertEquals(20, dao.getSeats().length);
        assertEquals(30, dao.getSeats()[0].length);

        // validate that the seats are instantiated
        assertTrue(dao.getSeat(0, 0).isOpen());
    }

    @Test
    public void testGetSetNumberOfColumns() {
        dao.initializeRepo(12, 24);
        assertEquals(12, dao.getNumberOfRows());
        assertEquals(24, dao.getNumberOfColumns());
    }

    @Test
    public void testDoForEachSeat() {
        Set<Coordinate> allCoordinates = new HashSet<>();
        for (int i = 0; i < dao.getNumberOfRows(); i++) {
            for (int j = 0; j < dao.getNumberOfColumns(); j++) {
                Coordinate coord = new Coordinate().setRow(i).setColumn(j);
                allCoordinates.add(coord);
            }
        }
        int numMaxSeats = ApplicationConfig.ROWS * ApplicationConfig.COLUMNS;
        assertEquals(numMaxSeats, allCoordinates.size());

        dao.doForEachSeat((seat) -> {
            Coordinate coord = new Coordinate().setRow(seat.getRow()).setColumn(seat.getColumn());
            allCoordinates.remove(coord);
        });
        assertEquals(0, allCoordinates.size());
    }

    @Test
    public void testGetSeatHolds() {
        assertEquals(0, dao.getSeatHolds().size());

        List<Seat> firstSeats = Arrays.asList(dao.getSeat(1, 1));
        SeatHold firstHold = dao.newSeatHold("a@b.com", firstSeats);
        List<Seat> secondSeats = Arrays.asList(dao.getSeat(2, 2));
        SeatHold secondHold = dao.newSeatHold("c@d.com", secondSeats);

        Map<Integer, SeatHold> seatHolds = dao.getSeatHolds();
        assertEquals(2, seatHolds.size());
        SeatHold resultFirst = seatHolds.get(firstHold.getId());
        assertEquals(firstHold, resultFirst);
        SeatHold resultSecond = seatHolds.get(secondHold.getId());
        assertEquals(secondHold, resultSecond);

    }

    @Test
    public void testRemoveAllSeatHolds() {
        assertEquals(0, dao.getSeatHolds().size());

        List<Seat> firstSeats = Arrays.asList(dao.getSeat(1, 1));
        SeatHold firstHold = dao.newSeatHold("a@b.com", firstSeats);
        List<Seat> secondSeats = Arrays.asList(dao.getSeat(2, 2));
        SeatHold secondHold = dao.newSeatHold("c@d.com", secondSeats);

        List<Seat> thirdSeats = Arrays.asList(dao.getSeat(3, 3));
        SeatHold thirdHold = dao.newSeatHold("c@d.com", thirdSeats);

        Map<Integer, SeatHold> seatHolds = dao.getSeatHolds();
        assertEquals(3, seatHolds.size());

        dao.removeAllSeatHolds(Arrays.asList(firstHold.getId(), thirdHold.getId()));

        seatHolds = dao.getSeatHolds();
        assertEquals(1, seatHolds.size());

        assertEquals(secondHold, seatHolds.get(secondHold.getId()));
    }

    @Test
    public void testReclaimExpiredHolds() {
        List<Seat> firstSeats = Arrays.asList(dao.getSeat(1, 1));
        SeatHold firstHold = dao.newSeatHold("a@b.com", firstSeats);
        // set this guy to expire in the past
        firstHold.setExpirationTime(System.currentTimeMillis() - 10_000);

        List<Seat> secondSeats = Arrays.asList(dao.getSeat(2, 2));
        SeatHold secondHold = dao.newSeatHold("c@d.com", secondSeats);

        Map<Integer, SeatHold> seatHolds = dao.getSeatHolds();
        assertEquals(2, seatHolds.size());

        // reclaim the expired holds
        dao.reclaimExpiredHolds();

        seatHolds = dao.getSeatHolds();
        assertEquals(1, seatHolds.size());

        SeatHold result = seatHolds.get(secondHold.getId());
        assertEquals(secondHold, result);
    }

    @Test
    public void testGetSeatHold() {
        // This shouldn't exist
        SeatHold hold = dao.getSeatHold(43);
        assertNull(hold);

        List<Seat> seats = Arrays.asList(dao.getSeat(1, 1));
        SeatHold seatHold = dao.newSeatHold("a@b.com", seats);

        assertEquals(seatHold, dao.getSeatHold(seatHold.getId()));
    }

    @Test
    public void testRemoveSeatHold() {
        List<Seat> seats = Arrays.asList(dao.getSeat(1, 1));
        SeatHold seatHold = dao.newSeatHold("a@b.com", seats);

        assertEquals(seatHold, dao.getSeatHold(seatHold.getId()));

        dao.removeSeatHold(seatHold.getId());

        assertEquals(0, dao.getSeatHolds().size());
    }

    @Test
    public void testGetAndPutReservationConfirmation() {
        // null case
        assertNull(dao.getReservationConfirmation("doesntExist"));

        // Verify the saving and getting of a reservation
        ReservationConfirmation confirm = new ReservationConfirmation();
        confirm.setEmail("me@boo.com");
        confirm.setId("barbar");
        dao.saveReservationConfirmation(confirm);

        assertEquals(confirm, dao.getReservationConfirmation("barbar"));
    }

}
