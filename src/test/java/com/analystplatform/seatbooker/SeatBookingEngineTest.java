package com.analystplatform.seatbooker;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Before;
import org.junit.Test;

import com.analystplatform.seatbooker.models.ReservationConfirmation;
import com.analystplatform.seatbooker.models.SeatHold;

public class SeatBookingEngineTest {

    private SeatBookingEngine engine;

    @Before
    public void setup() {
        engine = new SeatBookingEngine();
    }

    @Test
    public void testNumSeatsAvailable() {
        // By default, we expect everything to be open
        int expectedDefaultOpenSeats = ApplicationConfig.ROWS * ApplicationConfig.COLUMNS;
        assertEquals(expectedDefaultOpenSeats, engine.numSeatsAvailable());

        // Making some seats unavailable and assert the implementation correctly reflects it

        // One seat set to held
        engine.dao.getSeat(0, 0).setHeld(true);
        // One seat set to reserved
        engine.dao.getSeat(0, 1).setReserved(true);
        // One seat set to held and reserved
        engine.dao.getSeat(1, 1).setHeld(true);
        engine.dao.getSeat(1, 1).setReserved(true);
        // Expect the number to be 3 less than where we were before
        assertEquals(expectedDefaultOpenSeats - 3, engine.numSeatsAvailable());
    }

    @Test
    public void testFindAndHoldSeats() {
        int maxSeats = ApplicationConfig.ROWS * ApplicationConfig.COLUMNS;

        engine.findAndHoldSeats(5, "five@five.com");
        assertEquals(5, maxSeats - engine.numSeatsAvailable());

        // 5 + 15 = 20 expected seats reserved
        engine.findAndHoldSeats(15, "fifteen@fifteen.com");
        assertEquals(20, maxSeats - engine.numSeatsAvailable());

        // 20 + 30 = 50 expected seats reserved
        engine.findAndHoldSeats(30, "thirty@thirty.com");
        assertEquals(50, maxSeats - engine.numSeatsAvailable());

        // 50 + 500 = 550 expected seats reserved
        engine.findAndHoldSeats(500, "fiveHundo@fiveHundo.com");
        assertEquals(550, maxSeats - engine.numSeatsAvailable());

        // 550 + 211 = 761 expected seats reserved
        engine.findAndHoldSeats(211, "twooneone@twooneone.com");
        assertEquals(761, maxSeats - engine.numSeatsAvailable());

        // try filling up more seats than the venue contains
        engine.findAndHoldSeats(maxSeats + 1, "maxPlusOne@maxPlusOne.com");
        assertEquals(maxSeats, maxSeats - engine.numSeatsAvailable());
    }

    @Test
    public void testFindAndHoldSeats_BadInput() {
        int maxSeats = ApplicationConfig.ROWS * ApplicationConfig.COLUMNS;

        engine.findAndHoldSeats(0, "zero@0.com");
        assertEquals(0, maxSeats - engine.numSeatsAvailable());

        engine.findAndHoldSeats(-1, "negative@0.com");
        assertEquals(0, maxSeats - engine.numSeatsAvailable());
    }

    @Test
    public void testReclaimExpiredHolds_KicksOffEachSecond() throws Exception {
        int maxSeats = ApplicationConfig.ROWS * ApplicationConfig.COLUMNS;

        // first reserve 4 seats
        SeatHold heldSeat = engine.findAndHoldSeats(4, "me@me.com");
        assertEquals(1, engine.dao.getSeatHolds().size());

        // Expire the hold
        heldSeat.setExpirationTime(System.currentTimeMillis() - 10_000);

        // Wait a second for the expiration checker to kick off
        Thread.sleep(1111);

        // Expect our seats to be reclaimed
        assertEquals(0, maxSeats - engine.numSeatsAvailable());
        assertEquals(0, engine.dao.getSeatHolds().size());
    }

    @Test
    public void testReserveSeats() {
        int maxSeats = ApplicationConfig.ROWS * ApplicationConfig.COLUMNS;

        // Reserve 12 seats
        SeatHold heldSeat = engine.findAndHoldSeats(12, "bar@bar.com");

        // Verify the expected count of each seat type
        assertCounts(maxSeats - 12, 12, 0);

        // Verify the message for a Bad ID
        String expectedErrorBadId = engine.reserveSeats(12, "foo@bar.com");
        assertEquals(SeatBookingEngine.HOLD_NOT_FOUND_MESSAGE, expectedErrorBadId);

        // Verify the message for a mismatched email
        String expectedErrorWrongCustomer = engine.reserveSeats(heldSeat.getId(), "foo@bar.com");
        assertEquals(SeatBookingEngine.HOLD_FOR_DIFFERENT_CUSTOMER_MESSAGE, expectedErrorWrongCustomer);

        // Success! You've reserved the seats
        String confirmationNumber = engine.reserveSeats(heldSeat.getId(), "bar@bar.com");

        // Verify the expected count of each seat type
        assertCounts(maxSeats - 12, 0, 12);

        // Verify the confirmationNumber works and the seats match those reserved
        ReservationConfirmation repopulated = engine.dao.getReservationConfirmation(confirmationNumber);
        assertEquals(confirmationNumber, repopulated.getId());
        assertEquals(heldSeat.getSeats(), repopulated.getSeats());
        assertEquals("bar@bar.com", repopulated.getEmail());

    }

    private void assertCounts(int expectedOpen, int expectedHeld, int expectedReserved) {
        AtomicInteger heldCount = new AtomicInteger();
        AtomicInteger reservedCount = new AtomicInteger();
        AtomicInteger openCount = new AtomicInteger();
        engine.dao.doForEachSeat((seat) -> {
            if (seat.isHeld()) {
                heldCount.incrementAndGet();
            }
            if (seat.isReserved()) {
                reservedCount.incrementAndGet();
            }
            if (seat.isOpen()) {
                openCount.incrementAndGet();
            }
        });
        assertEquals(expectedOpen, openCount.get());
        assertEquals(expectedHeld, heldCount.get());
        assertEquals(expectedReserved, reservedCount.get());
    }

}
